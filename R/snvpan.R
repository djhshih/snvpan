#' Single Nucleotide Variant Pan
#'
#' A pan for separating true single nucleotide variants from artifacts
#' or unwanted variants.
#'
#' @docType package
#' @name snvpan
#' @importFrom Rcpp evalCpp
#' @import Rsamtools
#' @useDynLib snvpan
NULL
