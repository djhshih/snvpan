# Single Nucleotide Variant Pan #

A R package for separating true single nucleotide variants from artifacts or unwanted variants.
This package is in *early development*, and currently consists of several utility functions useful for variant filtering.
Some functions are implemented in C++ for speed using the Rcpp interface.


### Installation ###

Generate the data files by
```
cd data-raw
Rscript make_chrom_info.R
```

Install the package by
```
library(devtools)
document()
install()
```

### Usage ###

View the reference manual for documentation of available functions.
